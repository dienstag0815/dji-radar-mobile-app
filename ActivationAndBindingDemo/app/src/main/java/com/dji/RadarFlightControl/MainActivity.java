package com.dji.RadarFlightControl;

import android.app.Service;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Locale;

import dji.common.battery.BatteryState;
import dji.common.error.DJIError;
import dji.common.flightcontroller.FlightControllerState;
import dji.common.util.CommonCallbacks;
import dji.sdk.camera.VideoFeeder;
import dji.sdk.codec.DJICodecManager;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.products.Aircraft;
import dji.sdk.sdkmanager.DJISDKManager;


public class MainActivity extends AppCompatActivity implements View.OnClickListener,TextureView.SurfaceTextureListener {

    //FPV
    private VideoFeeder.VideoDataCallback receivedVideoDataCallback = null;
    private DJICodecManager codecManager = null;

    private static final int sleeptimems = 13;
    private static final String TAG = MainActivity.class.getName();
    String expectedAckMesage;
    //Declaration for Sending & Receiving
    protected Button btnSendDataToOSDK; //Sending GoTo Flightlevel
    protected Button btnStop; //Sending GoTo Flightlevel
    protected Switch switchRadar1; //Startstop Radar
    protected Switch switchHeigthLimit; //Startstop Radar
    protected Switch switchFrontLimit; //Startstop ForwardTracking
    protected TextView textDebugLine01; //Counter RX
    protected TextView textDebugLine02; //showing Messages
    protected TextView textRXACK; //showing MessagestextRXAck
    protected TextView textViewScrollData; //Telemetry
    protected EditText editTextHeigth1; //Enter Flightlevel in m
    protected EditText editTextLimitMin; //Enter Limit Min
    protected EditText editTextLimitMax; //Enter Limit Max

    //statusbar Textviews
    protected TextView textViewStatusBattery1;
    protected TextView textViewStatusFlightStatus1;
    protected TextView textViewStatusOSDK1;
    protected TextView textViewStatusOSDKCtrl;
    protected TextView textViewStatusLineScroll;
    protected TextView textViewStatusLoggingRadar;

    //Radar + Heigth Data textviews
    protected TextView textViewRadarDistance;
    protected TextView textViewRadarAngle;
    protected TextView textViewRadarSpeed;
    protected TextView textViewBaroHeigth;
    protected Switch switchGPSRadar;
    protected Switch switchRadarDownOrFront;

    Aircraft mAircraft = (Aircraft) DJISDKManager.getInstance().getProduct();
    FlightController mFlightController = mAircraft.getFlightController();
    FlightControllerState mFlightControllerState = mFlightController.getState();

    int i;
    int iRadarTmp;
    int j;
    int k;
    int k_tmp;
    int BatteryVolt;
    int RXCount =0;
    String[] RXStringArray = new String[10];
    float radarDistanceGlobal = 1;
    float radarAngleGlobal = 1;

    int xx;
    int yy;
    int secTimer;
    private LineGraphSeries<DataPoint> mSeriesRange;
    private LineGraphSeries<DataPoint> mSeriesAngle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        initUI();
        initFPVUI();

        receiveData();
        updateBattery();

        updateRX();
        sendDataToOSDK("STOPALL, \n");
        }

    public void updateRX(){
        new Thread(new Runnable() {
            public void run() {
                while(true){
                    try {
                        Thread.sleep(sleeptimems);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    textDebugLine02.post(new Runnable() {
                        public void run()
                        {
                            i++;
                            yy++;
                            secTimer++;

                            //////STATUS UPDATES
                            while(RXCount>0)
                            {
                                parseInputRXString(RXStringArray[RXCount]);
                                updateStatus();
                                RXCount--;
                                }
                            textDebugLine01.setText("U" + i + " RX" + j + " RD" + k + " RXC" + RXCount);

                            //Refresh Plot with RealTimeData
                            //Datapoint 10Hhz
                            //Calc:
                            // (1000 / sleeptimems) = 1hz
                            //Refreshrate = 10Hz
                            int plotRefreshRate = ((1000/sleeptimems)/10);
                            if(yy>(plotRefreshRate))
                            {
                                xx++;
                                mSeriesRange.appendData(new DataPoint((float)xx/10,radarDistanceGlobal), true, 120);
                                mSeriesAngle.appendData(new DataPoint((float)xx/10, radarAngleGlobal), true, 121);
                                yy=0;
                            }

                            //checking status every 3 seconds second and
                            if(secTimer > (1000/sleeptimems)*3)
                            {
                                secTimer=0;
                                if(k_tmp == k)
                                {
                                    textViewStatusOSDK1.setTextColor(Color.RED);
                                    switchRadar1.setTextColor(Color.RED);
                                    switchRadar1.setChecked(false);
                                    switchHeigthLimit.setChecked(false);
                                    Toast.makeText(getApplicationContext(), "Lost Connection to Onboard-System",
                                            Toast.LENGTH_SHORT).show();
                                }
                                k_tmp=k;

                            }


                        }
                    });
                }
            }
        }).start();


    }

    private void updateStatus()
    {
        // Battery
        textViewStatusBattery1.setText(""+BatteryVolt);
        //OSD Updates
        // Check if OSDK is available, Green = available, Red = not Available
        if (mFlightController.isOnboardSDKDeviceAvailable())
        {
            textViewStatusOSDK1.setTextColor(Color.GREEN);
        }else
        {
            textViewStatusOSDK1.setTextColor(Color.RED);
        }

        // Flightcontrollerstatus
        if (mFlightControllerState.isFlying())
        {
            textViewStatusFlightStatus1.setTextColor(Color.RED);
            textViewStatusFlightStatus1.setText("Flying ");
        }else if(mFlightControllerState.areMotorsOn() && !(mFlightControllerState.isFlying()) )
        {
            textViewStatusFlightStatus1.setTextColor(Color.RED);
            textViewStatusFlightStatus1.setText("Armed ");
        }else if(!(mFlightControllerState.areMotorsOn()) && !(mFlightControllerState.isFlying()) )
        {
            textViewStatusFlightStatus1.setTextColor(Color.GREEN);
            textViewStatusFlightStatus1.setText("Land ");
        }
        //Baro Altitude
        float baroAltitude = mFlightControllerState.getAircraftLocation().getAltitude();
        textViewBaroHeigth.setText(String.format(Locale.ROOT,"%.1f", Float.valueOf(baroAltitude)));

    }

    private void parseInputRXString(String InputRXString)
    {
        //Just go Into Parsing if data is received
        // R=Radar Message
        // T=Telemetry / GPS Message
        if(InputRXString.length() > 2)
        {
            k++;
            String[] RXStringSplit = InputRXString.split(",");
            textRXACK.setText(InputRXString);
            if(RXStringSplit[0].equals("R"))
            {
                iRadarTmp++;
                String radarDistance = RXStringSplit[1];
                radarDistanceGlobal = Float.valueOf(RXStringSplit[1]);
                String radarAngle = RXStringSplit[2];
                Float radarAngleFloatCorrected = Float.valueOf(radarAngle)-100;
                radarAngleGlobal = radarAngleFloatCorrected;

                String radarSpeed = RXStringSplit[3];

                textViewRadarDistance.setText(radarDistance);
                textViewRadarAngle.setText(String.format(Locale.ROOT,"%.1f", Float.valueOf(radarAngleFloatCorrected)));
                textViewRadarSpeed.setText(radarSpeed);
            }

            else if(RXStringSplit[0].equals("T"))
            {
                String altitudeGPS = RXStringSplit[4];
            }

            else if(RXStringSplit[0].equals("OSDKCtrl"))
            {
                //textRXACK.setText(RXString);
                String OSDKControl = RXStringSplit[1];
                if(OSDKControl.equals("1")) {
                    textViewStatusOSDKCtrl.setTextColor(Color.GREEN);
                }
                if(OSDKControl.equals("0")) {
                    textViewStatusOSDKCtrl.setTextColor(Color.RED);
                }
            }
//            else if(RXStringSplit[0].equals("ACK"))
//            {
//                String AckMsg = InputRXString;
//                String AckHeader = RXStringSplit[1];
//            }
            else if(RXStringSplit[0].equals("StatusLine"))
            {
                String StatusLine = RXStringSplit[1];
                textViewStatusLineScroll.append(StatusLine);
            }
            else if(RXStringSplit[0].equals("StartStatus"))
            {
                        String startStatusRadar = RXStringSplit[1];
                        if(startStatusRadar.equals("1")) {
                            switchRadar1.setChecked(true);
                            } else switchRadar1.setChecked(false);

        //                String startStatusTelem = RXStringSplit[2];

                        String startStatusRadar1GPS0 = RXStringSplit[3];
                        if(startStatusRadar1GPS0.equals("1")) {
                            switchGPSRadar.setChecked(true);
                            } else switchGPSRadar.setChecked(false);

                        String startStatusLoggingRadar = RXStringSplit[4];
                        if(startStatusLoggingRadar.equals("1")) {
                            textViewStatusLoggingRadar.setTextColor(Color.GREEN);
                            } else textViewStatusLoggingRadar.setTextColor(Color.RED);

                        //String startStatusLoggingTelemetry = RXStringSplit[5];

                        String startStatusLimit = RXStringSplit[6];
                        if(startStatusLimit.equals("1")) {
                            switchHeigthLimit.setChecked(true);
                            } else switchHeigthLimit.setChecked(false);

                        String startStatusTracking = RXStringSplit[7];
                        if(startStatusTracking.equals("1")) {
                            switchFrontLimit.setChecked(true);
                            } else switchFrontLimit.setChecked(false);

                        String statusRadarConnected = RXStringSplit[8];
                        if(statusRadarConnected.equals("1")) {
                            //Radar is available
                            switchRadar1.setTextColor(Color.GREEN);
                            }else
                            {switchRadar1.setTextColor(Color.RED);}

                        String radarDownInstalled = RXStringSplit[9];
                        if(radarDownInstalled.equals("1")) {
                            switchRadarDownOrFront.setChecked(true);
                            } else switchRadarDownOrFront.setChecked(false);

//                        String radarFrontInstalled = RXStringSplit[10];
//                        if(radarFrontInstalled.equals("1")) {
//                            switchRadarDownOrFront.setChecked(false);
//                            } else switchRadarDownOrFront.setChecked(true);

            }

            String rxDelete="";
            InputRXString = rxDelete;
        }
        else
        {

        }

    }

    private void initUI(){

        ///Range Plot
        GraphView graphRange = (GraphView) findViewById(R.id.graph);
        mSeriesRange = new LineGraphSeries<DataPoint>();
        graphRange.addSeries(mSeriesRange);
        graphRange.getViewport().setYAxisBoundsManual(true);
        graphRange.getViewport().setMinY(0);
        graphRange.getViewport().setMaxY(10);
        graphRange.getViewport().setXAxisBoundsManual(true);
        graphRange.getViewport().setMinX(0);
        graphRange.getViewport().setMaxX(12);
        graphRange.getViewport().scrollToEnd();
        graphRange.getGridLabelRenderer().setTextSize(32);
        graphRange.getGridLabelRenderer().setHorizontalLabelsColor(0xFAFAFA);

        ///Range Plot
        GraphView graphAngle = (GraphView) findViewById(R.id.graphAngle);
        mSeriesAngle = new LineGraphSeries<DataPoint>();
        graphAngle.addSeries(mSeriesAngle);
        graphAngle.getViewport().setYAxisBoundsManual(true);
        graphAngle.getViewport().setMinY(-31);
        graphAngle.getViewport().setMaxY(31);
        graphAngle.getViewport().setXAxisBoundsManual(true);
        graphAngle.getViewport().setMinX(0);
        graphAngle.getViewport().setMaxX(12);
        graphAngle.getViewport().scrollToEnd();
        graphAngle.getGridLabelRenderer().setTextSize(32);
        graphAngle.getGridLabelRenderer().setHorizontalLabelsColor(0xFAFAFA);



        //Declaration
        btnSendDataToOSDK = (Button) findViewById(R.id.buttonSendData);
        btnSendDataToOSDK.setOnClickListener(this);

        //Declaration
        btnStop = (Button) findViewById(R.id.buttonStop);
        btnStop.setOnClickListener(this);

        switchRadar1 = (Switch) findViewById(R.id.switchRadar);
        switchRadar1.setText("Radar");
        switchRadar1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sendDataToOSDK("StartRadar,1,\n");
                } else {
                    sendDataToOSDK("StartRadar,0,\n");
                }
            }
        });

        switchRadarDownOrFront = (Switch) findViewById(R.id.switchRadarDownOrFront);
        switchRadarDownOrFront.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sendDataToOSDK("RadarInstalled,Front,\n");
                } else {
                    sendDataToOSDK("RadarInstalled,Down,\n");
                }
            }
        });

        switchFrontLimit = (Switch) findViewById(R.id.switchFrontTracking);
        switchFrontLimit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //Check if Input is Valid (< 0 or Empty)
                    if(editTextLimitMax.getText().toString().equals(""))
                    {
                        editTextLimitMax.setText("10");
                        Toast.makeText(MainActivity.this, "Maximum cant be empty", Toast.LENGTH_SHORT).show();
                    }
                    if(editTextLimitMin.getText().toString().equals(""))
                    {
                        editTextLimitMin.setText("5");
                        Toast.makeText(MainActivity.this, "Minimum cant be empty", Toast.LENGTH_SHORT).show();
                    }

                    String editTextMax = editTextLimitMax.getText().toString();
                    String editTextMin = editTextLimitMin.getText().toString();

                    //check if Min is smaller and Max greater
                    if((Integer.parseInt(editTextMin) >=((Integer.parseInt(editTextMax))-2) && (Integer.parseInt(editTextMin) >= 2)))
                    {

                        Integer newMinInt= Integer.parseInt(editTextMax)-2;
                        String newMin = newMinInt.toString();
                        editTextLimitMin.setText(newMin);

                        Toast.makeText(MainActivity.this, "Minimum heigth must be 2m or less from Maximum heigth", Toast.LENGTH_SHORT).show();
                        editTextMin = editTextLimitMin.getText().toString();
                    } else
                    if (Integer.parseInt(editTextMax) <=((Integer.parseInt(editTextMin))+2))
                    {
                        Integer newMaxInt= Integer.parseInt(editTextMin)+2;
                        String newMax = newMaxInt.toString();
                        editTextLimitMax.setText(newMax);

                        Toast.makeText(MainActivity.this, "Maximum heigth must be 2m or more from Minimum heigth", Toast.LENGTH_SHORT).show();
                        editTextMax = editTextLimitMax.getText().toString();
                    }

                    if(Integer.parseInt(editTextLimitMin.getText().toString())< 0)
                    {
                        editTextLimitMin.setText("0");
                        Toast.makeText(MainActivity.this, "Minimum must be at least 0", Toast.LENGTH_SHORT).show();
                    }
                    sendDataToOSDK("LimitFront," + editTextMin + "," + editTextMax + ",\n");
                } else {
                    sendDataToOSDK("LimitFront," + 0 + "," + 0 + ",\n");
                }
            }
        });



        switchHeigthLimit = (Switch) findViewById(R.id.switchSendLimit);
        switchHeigthLimit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //Check if Input is Valid (< 0 or Empty)
                    if(editTextLimitMax.getText().toString().equals(""))
                    {
                        editTextLimitMax.setText("10");
                        Toast.makeText(MainActivity.this, "Maximum cant be empty", Toast.LENGTH_SHORT).show();
                        }
                    if(editTextLimitMin.getText().toString().equals(""))
                    {
                        editTextLimitMin.setText("5");
                        Toast.makeText(MainActivity.this, "Minimum cant be empty", Toast.LENGTH_SHORT).show();
                        }

                    String editTextMax = editTextLimitMax.getText().toString();
                    String editTextMin = editTextLimitMin.getText().toString();

                    //check if Min is smaller and Max greater
                    if((Integer.parseInt(editTextMin) >=((Integer.parseInt(editTextMax))-2) && (Integer.parseInt(editTextMin) >= 2)))
                    {

                        Integer newMinInt= Integer.parseInt(editTextMax)-2;
                        String newMin = newMinInt.toString();
                        editTextLimitMin.setText(newMin);

                        Toast.makeText(MainActivity.this, "Minimum heigth must be 2m or less from Maximum heigth", Toast.LENGTH_SHORT).show();
                        editTextMin = editTextLimitMin.getText().toString();
                        } else
                            if (Integer.parseInt(editTextMax) <=((Integer.parseInt(editTextMin))+2))
                            {
                                Integer newMaxInt= Integer.parseInt(editTextMin)+2;
                                String newMax = newMaxInt.toString();
                                editTextLimitMax.setText(newMax);

                                Toast.makeText(MainActivity.this, "Maximum heigth must be 2m or more from Minimum heigth", Toast.LENGTH_SHORT).show();
                                editTextMax = editTextLimitMax.getText().toString();
                                }

                    if(Integer.parseInt(editTextLimitMin.getText().toString())< 0)
                    {
                        editTextLimitMin.setText("0");
                        Toast.makeText(MainActivity.this, "Minimum must be at least 0", Toast.LENGTH_SHORT).show();
                    }
                    sendDataToOSDK("LimitDown," + editTextMin + "," + editTextMax + ",\n");
                } else {
                    sendDataToOSDK("LimitDown," + 0 + "," + 0 + ",\n");
                }
            }
        });

        textDebugLine01 = (TextView) findViewById(R.id.textViewDebug01);
        textDebugLine02 = (TextView) findViewById(R.id.textViewDebug);
        textRXACK = (TextView) findViewById(R.id.textViewRXACK);
        editTextHeigth1 = (EditText) findViewById(R.id.editTextHeigth);
        editTextLimitMax = (EditText) findViewById(R.id.editTextMaxHeigth);
        editTextLimitMin = (EditText) findViewById(R.id.editTextMinHeigth);

        //Status Bar & Corner
        textViewStatusLineScroll = (TextView) findViewById(R.id.textViewStautsLineScroll);
        textViewStatusLineScroll.setMovementMethod(new ScrollingMovementMethod());

        textViewStatusLoggingRadar = (TextView) findViewById(R.id.textViewLoggingRadar);
        textViewStatusBattery1 = (TextView) findViewById(R.id.textViewStatusBattery);
        textViewStatusFlightStatus1 = (TextView) findViewById(R.id.textViewStatusFlight);
        textViewStatusOSDK1 = (TextView) findViewById(R.id.textViewStatusOSDK);
        textViewStatusOSDKCtrl = (TextView) findViewById(R.id.textViewStatusOSDKCtrl);
//        textViewStatusBatteryCells = (TextView) findViewById(R.id.textViewBatterycells);
//        textViewStatusBatteryCells.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                if ("Cells:4 (Simulator)".equals(textViewStatusBatteryCells.getText().toString()))
//                {
//                    textViewStatusBatteryCells.setText("Cells:6 (Flying)");
//                    mAircraft.getBattery().setNumberOfCells(6,new CommonCallbacks.CompletionCallback() {
//                        @Override
//                        public void onResult(DJIError error) {
//                        }
//                    });
//                }
//
//                else if ("Cells:6 (Flying)".equals(textViewStatusBatteryCells.getText().toString()))
//                {
//                    textViewStatusBatteryCells.setText("Cells:4 (Simulator)");
//                    mAircraft.getBattery().setNumberOfCells(4,new CommonCallbacks.CompletionCallback() {
//                        @Override
//                        public void onResult(DJIError error) {
//                        }
//                    });
//                }
//            }
//        });
//
//        textViewStatusBatteryCells.setText("Cells:6");
//        mAircraft.getBattery().setNumberOfCells(6,new CommonCallbacks.CompletionCallback() {
//            @Override
//            public void onResult(DJIError error) {
//            }
//        });
        //Radar + HeigthData textviews
        textViewRadarDistance = (TextView) findViewById(R.id.textViewHeightRadar);
        textViewRadarAngle = (TextView) findViewById(R.id.textViewRadarAngle);
        textViewRadarSpeed = (TextView) findViewById(R.id.textViewRadarSpeed);
        textViewBaroHeigth = (TextView) findViewById(R.id.textViewHeightBaro);

        //GPS / Radar Data switch
        switchGPSRadar = (Switch) findViewById(R.id.switchGPSRadar);
        switchGPSRadar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)sendDataToOSDK("UseRadar,1\n");
                else{sendDataToOSDK("UseRadar,0\n");}
            }
        });

    }
    private void updateBattery(){
    mAircraft.getBattery().setStateCallback(new BatteryState.Callback(){
            @Override
            public void onUpdate(BatteryState batteryState) {
                    BatteryVolt = batteryState.getVoltage();
            }
        });
    }

    private void receiveData(){
                   ////Receive Data from Drone to Mobile - Data Transparent protocoll
                   mFlightController.setOnboardSDKDeviceDataCallback(new FlightController.OnboardSDKDeviceDataCallback() {
                        @Override
                        public void onReceive(byte[] bytes) {
                          j++;
                          RXCount++;
                          RXStringArray[RXCount] = new String(bytes);
                        }
                    });
    }

    private void sendDataToOSDK(String InputMessage)
    {
        textDebugLine02.setText(InputMessage);
        //Send Data
        mFlightController.sendDataToOnboardSDKDevice(InputMessage.getBytes(), new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError error) {
            }
        });
        //Set Ack Message
        expectedAckMesage = "ACK,"+ InputMessage;
    }
    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        //setUpListener();
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");
       super.onDestroy();
    }

    @Override

    public void onClick(View v) {

        switch (v.getId()) {

            //btnSendDataToOSDK = Goto Button
            case R.id.buttonSendData:{

                sendDataToOSDK("GoTo," + editTextHeigth1.getText().toString()+ "\n");


                break;
            }

            //btnSendDataToOSDK = Goto Button
            case R.id.buttonStop:{
                sendDataToOSDK("STOPALL, \n");
                break;
            }

            default:
                break;
        }
    }

////Videofeed Related Classes
    private void initFPVUI() {
        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
        //layoutInflater.inflate(R.layout.activity_main, this, true);

        Log.v("TAG", "Start to test");

        TextureView mVideoSurface = (TextureView) findViewById(R.id.textureViewFPV);

        if (null != mVideoSurface) {
            mVideoSurface.setSurfaceTextureListener(this);

            // This callback is for

            receivedVideoDataCallback = new VideoFeeder.VideoDataCallback() {
                @Override
                public void onReceive(byte[] bytes, int size) {
                    if (null != codecManager) {
                        codecManager.sendDataToDecoder(bytes, size);
                    }
                }
            };
        }

        initSDKCallback();
    }

    private void initSDKCallback() {
        try {
            VideoFeeder.getInstance().getPrimaryVideoFeed().setCallback(receivedVideoDataCallback);
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        if (codecManager == null) {
            codecManager = new DJICodecManager(this, surface, width, height);
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        if (codecManager != null) {
            codecManager.cleanSurface();
            codecManager = null;
        }
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }
}
